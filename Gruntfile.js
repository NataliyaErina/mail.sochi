module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            html: {
                files: ['src/*.html', 'src/*/*.html'],
                tasks: ['inlinecss']
            },
            css: {
                files: ['src/*.css', 'src/*/*.css'],
                tasks: ['inlinecss']
            }
        },
        inlinecss: {
            main: {
                options: {
                    webResources: {
                        images: false
                    }
                },
                files: {
                    'www/train/1.html': 'src/ikea-poezd/1.html',
                    'www/train/2.html': 'src/ikea-poezd/2.html',
                    'www/train/3.html': 'src/ikea-poezd/3.html',
                    'www/train/4.html': 'src/ikea-poezd/4.html',
                    'www/train/5.html': 'src/ikea-poezd/5.html',
                    'www/kolibel/index.html': 'src/ikea-kolibel/index.html',
                    'www/f1-2019-02-14/4.html': 'src/f1-2019-02-14/1.html',
                    'www/f1-2019-02-21/index.html': 'src/f1-2019-02-21/index.html',
                    'www/f1-2019-03-01/index.html': 'src/f1-2019-03-01/index (2).html',
                    'www/f1-2019-03-08/index.html': 'src/f1-2019-03-08/1.html',
                    'www/f1-2019-03-13/index.html': 'src/f1-2019-03-13/index.html',

                    'www/uaz/uaz-patriot-2019-26-02.html': 'src/uaz/uaz-patriot-2019-26-02.html',
                    'www/uaz/uaz-profi-2019-26-02.html': 'src/uaz/uaz-profi-2019-26-02.html',
                    'www/uaz/2019-03-04.html': 'src/uaz/uaz-life-style-2019-03-04.html',
                    'www/uaz/uazRoadShow2019.html': 'src/uaz/uazRoadShow2019.html'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-inline-css');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch', 'inlinecss']);

};